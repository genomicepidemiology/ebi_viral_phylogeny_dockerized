#!/bin/bash

####################################################################
#   General use script for running phylogenetic analysis in EBI    #
####################################################################


#################
#   Variables   #
#################

# Analysis paths
ANALYSIS_DIR="/analysis"
DATA="/data/input-data"
DATABASE="/analysis/database.sqlite"
OUTPUT="/analysis/dcc_private"

## Derived variables, do NOT change
SUFFIX=$(date +"%Y%m%d-%H")
LOGFILE="${OUTPUT}/${SUFFIX}.log"

###############
#   Scripts   #
###############

# download data
mkdir -p ${DATA}
mkdir -p ${OUTPUT}
if [ -z "$DCC_USER" ];
then
    sample_json_download.py -u "${URL}" -o ${DATA} -d ${DATABASE} -s 40 -a >> ${LOGFILE}
else
    sample_json_download.py -u "${URL}" -o ${DATA} -d ${DATABASE} -s 40 >> ${LOGFILE}
fi

# data processing
vu_pipeline.py -b ${ANALYSIS_DIR} -o ${OUTPUT} -d ${DATABASE} -m ${REFERENCE} -t ${SIMILARITY} -pairwise -p ${THREADS} -ebi -suffix ${SUFFIX} >> ${LOGFILE}

# uploading data
if [ ! -z "$DCC_USER" ];
then
    if [ ! -z "$WEBIN_USER" ];
    then
        # submit directly the results on private data back to its own project
        data_hub_result_submission.py --suffix ${SUFFIX} --ena-project ${PROJECT} --odir ${OUTPUT} --database ${DATABASE} --authenticate
    else
        # bundle results for later submission
        data_hub_result_submission.py --suffix ${SUFFIX} --ena-project ${PROJECT} --odir ${OUTPUT} --database ${DATABASE} --config /app/src/ena_kh_config.yaml --bundle --authenticate
        cp ${OUTPUT}/ena-submission_${SUFFIX}.tar /output_volume/
        cp ${OUTPUT}/ena-submission_${SUFFIX}.json /output_volume/
    fi
else
    if [ ! -z "$WEBIN_USER" ];
    then
        # submit directly the results
        data_hub_result_submission.py --suffix ${SUFFIX} --ena-project ${PROJECT} --odir ${OUTPUT} --database ${DATABASE}
    else
        # bundle results for later submission
        data_hub_result_submission.py --suffix ${SUFFIX} --ena-project ${PROJECT} --odir ${OUTPUT} --database ${DATABASE} --config /app/src/ena_kh_config.yaml --bundle
        cp ${OUTPUT}/ena-submission_${SUFFIX}.tar /output_volume/
        cp ${OUTPUT}/ena-submission_${SUFFIX}.json /output_volume/
    fi
fi
