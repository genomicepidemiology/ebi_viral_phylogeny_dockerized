FROM ubuntu:latest

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
    && apt-get install -y python3-pip python3-dev \
    && cd /usr/local/bin \
    && ln -s /usr/bin/python3 python \
    && pip3 install --upgrade pip

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE=1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED=1

# Install pip requirements
COPY requirements.txt .
RUN python -m pip install -r requirements.txt

WORKDIR /app
COPY . /app

# Set user
USER root

# Install git and wget
RUN apt-get update && apt-get install -y git wget curl vim

# Pull repositories
ENV OPENSSL_CONF="/app/openssl.level1.cnf"
RUN wget -c https://github.com/Cibiv/IQ-TREE/releases/download/v1.6.12/iqtree-1.6.12-Linux.tar.gz \
    && tar -xf iqtree-1.6.12-Linux.tar.gz \
    && mv iqtree-1.6.12-Linux/bin/iqtree /usr/local/bin/ && rm -rf iqtree-1.6.12-Linux*

RUN git clone --depth 1 https://github.com/enasequence/ena-analysis-submitter.git \
    && chmod +x ena-analysis-submitter/bin/*.py \
    && mv ./src/ena_kh_config.yaml ena-analysis-submitter/bin/config.yaml
RUN git clone --depth 1 --recurse-submodules https://bitbucket.org/genomicepidemiology/ebi_viral_phylogeny.git
RUN git clone --depth 1 https://bitbucket.org/jszarvas/consensus_variants.git

# Add to PATH
ENV PATH="$PATH:/app/ebi_viral_phylogeny/viral_surveillance:/app/ebi_viral_phylogeny:/app/consensus_variants:/app/ena-analysis-submitter/bin"

RUN git clone --depth 1 https://bitbucket.org/genomicepidemiology/kma.git \
    && cd kma \
    && make \
    && mv kma kma_index /usr/local/bin/ \
    && cd .. \
    && rm -rf ./kma

RUN git clone --depth 1 https://bitbucket.org/genomicepidemiology/ccphylo.git \
    && cd ccphylo \
    && make \
    && mv ccphylo /usr/local/bin/ \
    && cd .. \
    && rm -rf ./ccphylo

# Run bash script
RUN chmod +x src/pipeline.sh
CMD sh ./src/pipeline.sh
