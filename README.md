## Useful commands:

1. Build image: `$ docker-compose build`
2. Create and start container with env args and mounted volume: `$ docker run --name container_name -e var_name=value -v /home/path/reference:/reference phylo-analysis`
3. Delete container: `$ docker rm container_name`

## What's happening:
The **`Dockerfile`**:

1. Pulls ubuntu image
2. Installs python3
3. Installs requirements
4. Clones repositories
5. Sets entry point to the pipeline see: `src/pipeline.sh`

The **`pipeline.sh`** script:

1. Prepares additional variables
2. Downloads data using `sample_json_download.py` script
3. Processes data using `vu_pipeline.py` script
4. Prepares results for upload using `data_hub_result_submission.py` script

## Required environment arguments:
Argument which are needed when running `$ docker run...`

1. `REFERENCE` - reference sequence(s) in a .fasta file
2. `SIMILARITY` - sequence similarity threshold, percentage
3. `THREADS` - number of max parallel processes
4. `DCC_USER` - user name for the private hub (optional)
5. `DCC_PWD` - password for the private hub (optional)
6. `PROJECT` - ENA project accession of result
7. `URL` - Query url to EBI Portal API

Examples:

`REFERENCE` example: "/reference/NC_045512_2.fasta"

`SIMILARITY` example: "85.0"

`THREADS` example: "8"

`DCC_USER` example: "dcc_green"

`PROJECT` example: "PRJEB41789"

`URL` example:
"https://www.ebi.ac.uk/ena/portal/api/search?dataPortal=pathogen&query=tax_tree(2697049)&result=read_run&fields=collection_date,country,fastq_ftp&sortFields=collection_date&format=json&limit=0"

Full example:

`$ docker run --name pipeline_container -v /home/centos/reference:/reference -e REFERENCE="/reference/NC_045512_2.fasta" -e SIMILARITY="85.0" -e THREADS="8" -e DCC_USER="dcc_green" -e DCC_PWD="" -e PROJECT=PRJEB41789 -e URL="https://www.ebi.ac.uk/ena/portal/api/search?dataPortal=pathogen&query=tax_tree(2697049)&result=read_run&fields=collection_date,country,fastq_ftp&sortFields=collection_date&format=json&limit=0" phylo-analysis`
